 # TP n°1 :

Objectif : Virtualiser une machine physique sur un ESXi.

Prérequis : VMware Workstation

3 VM :
- 1 esxi
- 1 VM Windows
- 1 VM Linux

Toutes les machines dans le même réseau.



## 1. Installation de VMware vSphere Hypervisor (ou Esx 7)

1. Trouver les prérequis hardware pour installer un Esxi.
2. Installer l'esxi dans VMware Workstation
    - Mettez les ressources nécessaires pour faire tourner les VM que l'on va migrer. 
4. Suivez les instructions suivantes :
    - Avoir une IP statique
    - Insérer la licence
    - Ajouter un datastore (assez gros pour héberger quelques VM)

![partie1](images/partie1.PNG)

Pour cette partie on peut voir un GUI ESXi fonctionnel comprennant les 2 VM 

## 2. Utilisation de VMware vCenter Converter Standalone (mode local)

Pour cela, télécharger vCenter converter en local sur la VM.
Mettre la machine locale en remote sur notre serveur ESXi et la magie opère. 

![converter](./images/converter.PNG)

![winserv](./images/winserv.PNG)

![winfonctionne](./images/winfonctionne.PNG)

## 3. Utilisation de VMware vCenter Converter Standalone (mode Client-Server)

1. Crée une VM Linux.
2. Installer vCenter Converter sur votre PC ou sur le Windows migrer précédemment, en mode client-serveur.
3. Utiliser le pour migrer le serveur linux sur l'esxi.
4. Vérifier que la VM fonctionne bien dans votre ESXI

Meme chose qu'au dessus.
Mais choisir "client-server"

petit choix possible:
![clientserver](./images/serverclient1.PNG)


Question : 

Quel sont les différences entre les 2 modes ? Dans quel cas on va privilégié le mode local ? Et le mode Client-Server ?

L’installation client-serveur vous permet de sélectionner les composants du convertisseur que vous souhaitez installer sur votre système:

- Serveur
    Le serveur gère les tâches de conversion, gère la communication entre le convertisseur client et les agents de conversion. Ce composant ne peut pas être installé seul. En fait, le serveur de conversion doit être installé avec le module d’accès à distance ou avec le module client ou les deux.
- Acces à distance
    Si l’accès à distance est installé, les clients du convertisseur (locaux et distants) peuvent se connecter au serveur autonome du convertisseur local. Avec un accès à distance, vous pouvez créer et gérer des tâches de conversion à distance.
- Agent de conversion
    Installez l’agent de conversion de façon à ce que l’ordinateur local puisse être utilisé comme source pour les conversions.
- Convertisseur Client
    Si vous n’installez que le convertisseur client, vous pouvez vous connecter à un serveur convertisseur distant. Vous pouvez ensuite utiliser la machine distante pour convertir des machines virtuelles hébergées, des machines virtuelles gérées ou des machines physiques distantes.

L’installation locale installe tous les composants pour une utilisation locale (le serveur, l’agent de conversion et le client de conversion), aussi, le formulaire d’accès distant sera exclu.
